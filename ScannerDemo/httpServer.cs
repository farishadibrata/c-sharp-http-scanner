﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Threading.Tasks;
using ScannerDemo;

namespace httpServer
{

    public class httpServer
    {
        public string[] prefixes { get; set; } = { "http://127.0.0.1:3003/" };
        public Boolean isRunning { get; set; } = true;
        public Thread serverThread;
        public Form1 formUi;

        public httpServer(Form1 myForm)
        {
            formUi = myForm;
        }

        public string start()
        { 
            serverThread = new Thread(server);
            serverThread.Start();
            return "started";
        }
        
        public void stop()
        {
            serverThread.Abort();
        }

        public void server()
        {
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // Create a listener.
            HttpListener listener = new HttpListener();
            // Add the prefixes.
            foreach (string s in prefixes)
            {
                listener.Prefixes.Add(s);
            }
            listener.Start();
            while (isRunning)
            {
                HttpListenerContext context = listener.GetContext();
                HttpListenerResponse response = context.Response;
                HttpListenerRequest request = context.Request;
                response.Headers.Add("Access-Control-Allow-Origin", "*");
                response.Headers.Add("Access-Control-Allow-Methods", "POST, GET");
                string filePath = "";
                Task.Factory.StartNew(formUi.StartScanning).ContinueWith(result => filePath = formUi.TriggerScan()).ContinueWith(fileName =>
                 {
                     try
                     {
                         using (System.Drawing.Image image = System.Drawing.Image.FromFile(filePath))
                         {
                             using (MemoryStream m = new MemoryStream())
                             {
                                 image.Save(m, image.RawFormat);
                                 byte[] imageBytes = m.ToArray();

                                 // Convert byte[] to Base64 String
                                 string base64String = Convert.ToBase64String(imageBytes);
                                 string ext = formUi.imageExtension;
                                 ext = ext.Replace(".", string.Empty);
                                 string responseString = $"data:image/{ext};base64, " + base64String;
                                 byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                                 // Get a response stream and write the response to it.
                                 response.ContentLength64 = buffer.Length;
                                 System.IO.Stream output = response.OutputStream;
                                 output.Write(buffer, 0, buffer.Length);
                             }
                         }
                     }
                     catch
                     {
                         string responseString = "<HTML><BODY> Error!</BODY></HTML>";
                         byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                         // Get a response stream and write the response to it.
                         response.ContentLength64 = buffer.Length;
                         System.IO.Stream output = response.OutputStream;
                         output.Write(buffer, 0, buffer.Length);
                     }
                 });
                
               
            }
        }
        


    }
}
